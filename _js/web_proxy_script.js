window.onload = function () {
    var parentOrigin;

    function loadScript(url, callback) {

//        console.log(url);

        var script = document.createElement("script");
        script.type = "text/javascript";

        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" ||
                        script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    window.addEventListener("message", function (e)
    {
//        console.log(event.origin, parentOrigin);
        // Do we trust the sender of this message?
        if (e.origin !== parentOrigin) {
            return;
        }

//        console.log(e);
        //interprete the instruction
        switch (e.data.Command) {
            case 1:
            {
                var src = e.data.ScriptSource;

//                console.log("About to load script");
                loadScript(src, function () {
                    console.log("script loaded");
                    e.source.postMessage({
                        Command: 2,
                        Message: "Script Loaded"
                    }, parentOrigin);
                });

                break;
            }
            case 3:
            {
                var post = $.ajax(e.data.AjaxSettings);
                post.success(function (response) {
//                    console.log(response);
                    e.source.postMessage({
                        Command: 4,
                        Response: response,
                        Status: "success",
                        CallbackName: e.data.CallbackName
                    }, parentOrigin);
                }).error(function (response) {
//                    console.log(response);
                    e.source.postMessage({
                        Command: 4,
                        Response: {
                            responseText: response.responseText,
                            status: response.status,
                            statusText: response.statusText,
                            readyState: response.readyState
                        },
                        Status: "error",
                        CallbackName: e.data.CallbackName
                    }, parentOrigin);
                });
                break;
            }
            default:
            {
                console.log("Unknown instruction", e.data);
            }
        }

//        event.source.postMessage("hi there yourself!  the secret response " +
//                "is: rheeeeet!",
//                event.origin);
    }, false);

    parentOrigin = window.location.hash.replace(/^#/, '');
    window.parent.postMessage({
        Command: 0,
        Message: "I am ready"
    }, parentOrigin);
};