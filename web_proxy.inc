<?php

function web_proxy_call() {

    $remote_url = '';
    $options = "";
    $args = func_get_args();
    $arg_len = count($args);
//    echo $args[$arg_len - 1];
    $matches = "";
    if ($arg_len > 0 && preg_match("/(options\()([\w\d\/\.\=\s\(\);:\|]+)?(\))/", $args[$arg_len - 1], $matches)) {
        $options = str_replace("_x002F_", "/", $matches[2]);
        unset($args[$arg_len - 1]);
    }

    $remote_url = implode("/", $args);

    if (count($_GET) > 1) {
        $remote_url .="?";
        foreach ($_GET as $get => $val) {
            if ($get != "q") {
                $remote_url .="{$get}={$val}";
            }
        }
    }
//    echo $remote_url;

    $settings = array();
    $ops = explode("|", $options);
    foreach ($ops as $op) {
        $expression = "/([\w\d\_]+)(=)([\w\d\/\.\s\(\);:]+)/";
        $key = preg_replace($expression, "$1", $op);
        if (strlen($key) > 0) {
            $value = preg_replace($expression, "$3", $op);
            $settings[$key] = $value;
        }
    }


    // Change these configuration options if needed, see above descriptions for info.
    $enable_jsonp = true;
    $enable_native = false;

    //TODO: add code to list the allowed domains 
    $valid_url_regex = '/.*/';

    // ############################################################################

    if (strlen($remote_url) < 1) {

        // Passed url not specified.
        $contents = 'ERROR: url not specified';
        $status = array('http_code' => 'ERROR');
    } else if (!preg_match($valid_url_regex, $remote_url)) {

        // Passed url doesn't match $valid_url_regex.
        $contents = 'ERROR: invalid url';
        $status = array('http_code' => 'ERROR');
    } else {
        $ch = curl_init($remote_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
        }

        if ($settings['send_cookies']) {
            $cookie = array();
            foreach ($_COOKIE as $key => $value) {
                $cookie[] = $key . '=' . $value;
            }
            if ($settings['send_session']) {
                $cookie[] = SID;
            }
            $cookie = implode('; ', $cookie);

            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_USERAGENT, $settings['user_agent'] ? $settings['user_agent'] : $_SERVER['HTTP_USER_AGENT'] );

        $curl_exec = curl_exec($ch);
        list( $header, $contents ) = preg_split('/([\r\n][\r\n])\\1/', $curl_exec, 2);

//        echo $header;

        $status = curl_getinfo($ch);
//        echo var_dump($status);

        curl_close($ch);
    }

// Split header text into an array.
    $header_text = preg_split('/[\r\n]+/', $header);

    header('Access-Control-Allow-Origin: *');
//    header('X-Frame-Options: SAMEORIGIN');

    if ($settings['mode'] == 'native') {
        if (!$enable_native) {
            $contents = 'ERROR: invalid mode';
            $status = array('http_code' => 'ERROR');
        }

        // Propagate headers to response.
        foreach ($header_text as $header_item) {
            if (preg_match('/^(?:Content-Type|Content-Language|Set-Cookie):/i', $header_item)) {
                header($header_item);
            }
        }

        print $contents;
    } else {

        // $data will be serialized into JSON data.
        $data = array();

//        echo 'Header Count: ' . count($header). $header_text;
        // Propagate all HTTP headers into the JSON data object.
        if ($settings['full_headers']) {
            $data['headers'] = array();

            foreach ($header_text as $header_item) {
                preg_match('/^(.+?):\s+(.*)$/', $header_item, $matches);
                if ($matches) {
                    $data['headers'][$matches[1]] = $matches[2];
                }
            }
        }

        // Propagate all cURL request / response info to the JSON data object.
        if ($settings['full_status']) {
            $data['status'] = $status;
        } else {
            $data['status'] = array();
            $data['status']['http_code'] = $status['http_code'];
        }

        // Set the JSON data object contents, decoding it from JSON if possible.
        $decoded_json = json_decode($contents);
        $data['contents'] = $decoded_json ? $decoded_json : $contents;

        // Generate appropriate content-type header.
        $is_xhr = true; //strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
//        header('Content-Type: application/' . ( $is_xhr ? 'application/json; charset=UTF-8' : 'application/x-javascript' ));
        // Get JSONP callback.
        $jsonp_callback = $enable_jsonp && isset($settings['callback']) ? $settings['callback'] : null;

        // Generate JSON/JSONP string

        print $jsonp_callback ? "$jsonp_callback(" : "";
        drupal_json_output($data);
        print $jsonp_callback ? ")" : "";

//        $json = json_encode($data);
//        print $jsonp_callback ? "$jsonp_callback($json)" : $json;
    }
}
