<?php

function web_proxy_script_call() {
    header('Content-Type: text/html; charset=utf-8');
//	header ( 'Character-Encoding: utf-8' );	

    $b = drupal_get_path('module', 'web_proxy');
    $init = <<<"TET"
            <!DOCTYPE html>
            <html>
                <head>
                    <script type='text/javascript' src='/{$b}/_js/jquery-2.1.3.min.js'>
                        
                    </script>                    
                </head>
                <body>
                    <script type='text/javascript' src='/{$b}/_js/web_proxy_script.js'>
                        
                    </script>
                </body>
            </html>
TET;

//	header ( 'Content-Length: '.(strlen($init)) );
    print $init;
}
